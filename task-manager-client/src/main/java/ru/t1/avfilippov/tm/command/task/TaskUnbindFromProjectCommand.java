package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "unbind task from project";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.unbindTaskFromProject(request);
    }

}
