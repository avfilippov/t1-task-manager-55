package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.TaskStartByIdRequest;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "start task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken());
        request.setTaskId(id);
        taskEndpoint.startTaskById(request);
    }

}
