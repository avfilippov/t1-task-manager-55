package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.dto.request.TaskShowByProjectIdRequest;
import ru.t1.avfilippov.tm.dto.response.TaskShowByProjectIdResponse;
import ru.t1.avfilippov.tm.util.TerminalUtil;

import java.util.Collections;
import java.util.List;

@Component
public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "show task list by project id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final TaskShowByProjectIdResponse response = taskEndpoint.showTaskByProjectId(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        @NotNull final List<TaskDTO> tasks = response.getTasks();
        renderTasks(tasks);
    }

}
