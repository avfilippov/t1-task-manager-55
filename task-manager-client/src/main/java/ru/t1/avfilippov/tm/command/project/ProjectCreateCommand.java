package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ProjectCreateRequest;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "create new project";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        projectEndpoint.createProject(request);
    }

}
