package ru.t1.avfilippov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ServerVersionRequest;
import ru.t1.avfilippov.tm.dto.response.ServerVersionResponse;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show application version";
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.print("Client: ");
        System.out.println(propertyService.getApplicationVersion());
        System.out.print("Server: ");
        @Nullable final ServerVersionRequest request = new ServerVersionRequest(getToken());
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}
