package ru.t1.avfilippov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.avfilippov.tm.api.service.dto.ISessionDTOService;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, SessionDTORepository>
        implements ISessionDTOService {

    @Override
    @NotNull
    protected SessionDTORepository getRepository() {
        return context.getBean(SessionDTORepository.class);
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO add(@Nullable final SessionDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        if (model.getUserId() == null) throw new UserIdEmptyException();
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final SessionDTO result;
        try {
            entityManager.getTransaction().begin();
            repository.add(model.getUserId(), model);
            entityManager.getTransaction().commit();
            result = repository.findOneById(model.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

}
