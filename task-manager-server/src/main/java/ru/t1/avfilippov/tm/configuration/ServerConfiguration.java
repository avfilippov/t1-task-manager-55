package ru.t1.avfilippov.tm.configuration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Session;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ComponentScan("ru.t1.avfilippov.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    @SneakyThrows
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUser());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDBShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getDBFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getSecondLvlCache());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getFactoryClassCache());
        settings.put(Environment.DEFAULT_SCHEMA, propertyService.getDBSchemaName());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinPutsCache());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getRegionPrefixCache());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    private Connection getConnection(Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public EntityManager getEntityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public Liquibase getLiquibase() {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = getConnection(properties);
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @NotNull final Database dataBase = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        return new Liquibase(propertyService.getLiquibaseConfig(), accessor, dataBase);
    }

}
