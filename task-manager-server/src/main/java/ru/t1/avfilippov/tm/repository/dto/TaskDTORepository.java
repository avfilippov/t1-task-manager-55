package ru.t1.avfilippov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    @Override
    @NotNull
    protected Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }

    @Override
    @NotNull
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final TaskDTO task = new TaskDTO(name, description);
        return add(userId, task);
    }

    @Override
    @NotNull
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final TaskDTO task = new TaskDTO(name);
        return add(userId, task);
    }

    @Override
    @Nullable
    public List<TaskDTO> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName()
                + " m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
