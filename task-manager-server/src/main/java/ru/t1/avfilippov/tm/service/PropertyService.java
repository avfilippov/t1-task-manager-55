package ru.t1.avfilippov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "25421";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "dsad2312";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String PASSWORD_ITERATION = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET = "password.secret";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SERVER_HOST = "server.host";
    @NotNull
    public static final String EMPTY_VALUE = "---";
    @NotNull
    private static final String SESSION_KEY_DEFAULT = "111222";
    @NotNull
    private static final String SESSION_KEY = "session.key";
    @NotNull
    private static final String SESSION_TIMEOUT_KEY_DEFAULT = "8000";
    @NotNull
    private static final String SESSION_TIMEOUT = "session.timeout";
    @NotNull
    private static final String DB_USER_DEFAULT = "postgres";
    @NotNull
    private static final String DB_USER = "database.username";
    @NotNull
    private static final String DB_PASSWORD_DEFAULT = "postgres";
    @NotNull
    private static final String DB_PASSWORD = "database.password";
    @NotNull
    private static final String DB_URL_DEFAULT = "jdbc:postgresql://localhost:5432/tm";
    @NotNull
    private static final String DB_URL = "database.url";
    @NotNull
    private static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";
    @NotNull
    private static final String DB_DRIVER = "database.driver";
    @NotNull
    private static final String DB_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";
    @NotNull
    private static final String DB_DIALECT = "database.dialect";
    @NotNull
    private static final String DB_SHOW_SQL_DEFAULT = "false";
    @NotNull
    private static final String DB_SHOW_SQL = "database.show_sql";
    @NotNull
    private static final String DB_FORMAT_SQL_DEFAULT = "false";
    @NotNull
    private static final String DB_FORMAT_SQL = "database.format_sql";
    @NotNull
    private static final String DB_HBM2DDL_AUTO_DEFAULT = "update";
    @NotNull
    private static final String DB_HBM2DDL_AUTO = "database.hbm2ddl_auto";
    @NotNull
    private static final String DB_2ND_LVL_CACHE_DEFAULT = "true";
    @NotNull
    private static final String DB_2ND_LVL_CACHE = "database.second_lvl_cache";
    @NotNull
    private static final String DB_FACTORY_CLASS_DEFAULT = "com.hazelcast.hibernate.HazelcastCacheRegionFactory";
    @NotNull
    private static final String DB_FACTORY_CLASS = "database.factory_class";
    @NotNull
    private static final String DB_USE_QUERY_CACHE_DEFAULT = "true";
    @NotNull
    private static final String DB_USE_QUERY_CACHE = "database.use_query_cache";
    @NotNull
    private static final String DB_USE_MIN_PUTS_DEFAULT = "true";
    @NotNull
    private static final String DB_USE_MIN_PUTS = "database.use_min_puts";
    @NotNull
    private static final String DB_REGION_PREFIX_CACHE_DEFAULT = "tm";
    @NotNull
    private static final String DB_REGION_PREFIX_CACHE = "database.region_prefix";
    @NotNull
    private static final String DB_CONFIG_FILE_PATH_CACHE_DEFAULT = "hazelcast.xml";
    @NotNull
    private static final String DB_CONFIG_FILE_PATH_CACHE = "database.config_file_path";
    @NotNull
    private static final String DB_SCHEMA_NAME_DEFAULT = "public";
    @NotNull
    private static final String DB_SCHEMA_NAME = "database.default_schema";
    @NotNull
    private static final String INIT_TOKEN = "token.init";
    @NotNull
    private static final String LIQUIBASE_CONFIG = "database.liquibase_config";
    @NotNull
    private static final String LIQUIBASE_CONFIG_DEFAULT = "changelog/changelog-master.xml";
    @NotNull
    private static final String JMS_URL = "jms.url";
    @NotNull
    private static final String JMS_URL_DEFAULT = "tcp://localhost:61616";
    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }


    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @Override
    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @Override
    @NotNull
    public int getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_KEY_DEFAULT);
    }

    @Override
    @NotNull
    public String getDatabaseUser() {
        return getStringValue(DB_USER, DB_USER_DEFAULT);
    }

    @Override
    @NotNull
    public String getDatabasePassword() {
        return getStringValue(DB_PASSWORD, DB_PASSWORD_DEFAULT);
    }

    @Override
    @NotNull
    public String getDatabaseUrl() {
        return getStringValue(DB_URL, DB_URL_DEFAULT);
    }

    @Override
    @NotNull
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER, DB_DRIVER_DEFAULT);
    }

    @Override
    @NotNull
    public String getDatabaseDialect() {
        return getStringValue(DB_DIALECT, DB_DIALECT_DEFAULT);
    }

    @Override
    @NotNull
    public String getDBHbm2ddlAuto() {
        return getStringValue(DB_HBM2DDL_AUTO, DB_HBM2DDL_AUTO_DEFAULT);
    }

    @Override
    @NotNull
    public String getDBShowSql() {
        return getStringValue(DB_SHOW_SQL, DB_SHOW_SQL_DEFAULT);
    }

    @Override
    @NotNull
    public String getDBFormatSql() {
        return getStringValue(DB_FORMAT_SQL, DB_FORMAT_SQL_DEFAULT);
    }

    @Override
    @NotNull
    public String getSecondLvlCache() {
        return getStringValue(DB_2ND_LVL_CACHE, DB_2ND_LVL_CACHE_DEFAULT);
    }

    @Override
    @NotNull
    public String getFactoryClassCache() {
        return getStringValue(DB_FACTORY_CLASS, DB_FACTORY_CLASS_DEFAULT);
    }

    @Override
    @NotNull
    public String getUseQueryCache() {
        return getStringValue(DB_USE_QUERY_CACHE, DB_USE_QUERY_CACHE_DEFAULT);
    }

    @Override
    @NotNull
    public String getUseMinPutsCache() {
        return getStringValue(DB_USE_MIN_PUTS, DB_USE_MIN_PUTS_DEFAULT);
    }

    @Override
    @NotNull
    public String getRegionPrefixCache() {
        return getStringValue(DB_REGION_PREFIX_CACHE, DB_REGION_PREFIX_CACHE_DEFAULT);
    }

    @Override
    @NotNull
    public String getCacheConfigFilePath() {
        return getStringValue(DB_CONFIG_FILE_PATH_CACHE, DB_CONFIG_FILE_PATH_CACHE_DEFAULT);
    }

    @Override
    @NotNull
    public String getDBSchemaName() {
        return getStringValue(DB_SCHEMA_NAME, DB_SCHEMA_NAME_DEFAULT);
    }

    @Override
    @NotNull
    public String getInitToken() {
        return getStringValue(INIT_TOKEN, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getLiquibaseConfig() {
        return getStringValue(LIQUIBASE_CONFIG, LIQUIBASE_CONFIG_DEFAULT);
    }

    @Override
    @NotNull
    public String getJmsURL() {
        return getStringValue(JMS_URL, JMS_URL_DEFAULT);
    }

}
