package ru.t1.avfilippov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.avfilippov.tm.api.endpoint.IAdminEndpoint;
import ru.t1.avfilippov.tm.api.service.IAdminService;
import ru.t1.avfilippov.tm.dto.request.SchemeDropRequest;
import ru.t1.avfilippov.tm.dto.request.SchemeInitRequest;
import ru.t1.avfilippov.tm.dto.response.SchemeDropResponse;
import ru.t1.avfilippov.tm.dto.response.SchemeInitResponse;
import ru.t1.avfilippov.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.IAdminEndpoint")
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private IAdminService adminService;

    @Override
    @NotNull
    @WebMethod
    public SchemeInitResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SchemeInitRequest request
    ) {
        try {
            adminService.initScheme(request.getToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeInitResponse();
    }

    @Override
    @NotNull
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SchemeDropRequest request
    ) {
        try {
            adminService.dropScheme(request.getToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeDropResponse();
    }

}
