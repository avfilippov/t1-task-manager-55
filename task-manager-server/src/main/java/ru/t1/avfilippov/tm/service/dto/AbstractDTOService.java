package ru.t1.avfilippov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.repository.dto.IDTORepository;
import ru.t1.avfilippov.tm.api.service.dto.IService;
import ru.t1.avfilippov.tm.dto.model.AbstractModelDTO;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IService<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        System.out.println("Session creating. service " + model.getClass().getSimpleName());
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear() {
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable List<M> findAll() {
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final M model) {
        if (model == null) return;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M model) {
        if (model == null) return;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(sort.getComparator());
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        @NotNull M model = findOneById(id);
        remove(model);
    }

}
