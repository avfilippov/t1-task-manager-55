package ru.t1.avfilippov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class ServerVersionRequest extends AbstractUserRequest {

    public ServerVersionRequest(@Nullable final String token) {
        super(token);
    }

}
